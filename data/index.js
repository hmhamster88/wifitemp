function getJson(url) {
  return new Promise(function(resolve, reject) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function()
    {
      if(xmlHttp.readyState == 4)
      {
        if(xmlHttp.status == 200) {
          resolve(JSON.parse(xmlHttp.responseText));
        } else {
          reject(xmlHttp);
        }
      }
    }
    xmlHttp.open('get', url);
    xmlHttp.send();
  });
}

function postJson(url, data) {
  return new Promise(function(resolve, reject) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function()
    {
      if(xmlHttp.readyState == 4)
      {
        if(xmlHttp.status == 200) {
          resolve(JSON.parse(xmlHttp.responseText));
        } else {
          reject(xmlHttp);
        }
      }
    }
    xmlHttp.open('post', url);
    xmlHttp.send(JSON.stringify(data));
  });
}

var valueGetters = {
  number () {
    return Number.parseInt(this.value);
  },
  string () {
    return this.value;
  },
  boolean() {
    return this.checked === 'true';
  }
}

const inputTypes = {
  boolean: 'checkbox'
}

function getInputType(value) {
  const typeName = typeof value;
  return inputTypes[typeName] || typeName;
}

function addField(fields, settings, propName, props) {
  var isArray = Array.isArray(settings);
  var propertyMeta = isArray ? propName : settings[propName];
  var container = document.createElement('div');
  fields.appendChild(container);
  if (propertyMeta.displayName) {
    var nameSpan = document.createElement('span');
    nameSpan.innerHTML = propertyMeta.displayName;
    container.appendChild(nameSpan);
    container.appendChild(document.createElement('br'));
  }
  if (typeof propertyMeta.value === "object") {
      var div = document.createElement('div');
      div.setAttribute('style', 'margin-left: 20px');
      updateFields(div, propertyMeta.value);
      container.appendChild(div);
  } else {
      var input = document.createElement('input');
      input.setAttribute('type', getInputType(propertyMeta.value));
      input.setAttribute('value', propertyMeta.value);
      input.setAttribute('name', propName);
      input.propertyMeta = propertyMeta;
      input.onchange = function () {
          this.propertyMeta.value = this.getVal();
      }
      input.getVal = valueGetters[typeof propertyMeta.value];
      container.appendChild(input)
  }
  if (isArray) {
    var deleteButton = document.createElement('input');
    deleteButton.setAttribute('type', 'button');
    deleteButton.setAttribute('value', 'Delete');
    deleteButton.propertyMeta = propertyMeta;
    deleteButton.settings = settings;
    deleteButton.container = container;
    deleteButton.fields = fields;
    deleteButton.onclick = function () {
      var index = this.settings.indexOf(this.propertyMeta);
      this.settings.splice(index, 1);
      this.fields.removeChild(this.container);
    }
    container.appendChild(deleteButton);
  }
}

function updateFields(fields, settings) {
  while (fields.firstChild) {
    fields.removeChild(fields.firstChild);
  }
  var isArray = Array.isArray(settings);
  var props = isArray ? settings : Object.getOwnPropertyNames(settings);
  for(var propName of props) {
    addField(fields, settings, propName, props);
  }
  if (isArray) {
    var addButton = document.createElement('input');
    addButton.setAttribute('type', 'button');
    addButton.setAttribute('value', 'Add');
    addButton.settings = settings;
    addButton.fields = fields;
    addButton.onclick = function () {
      this.settings.push(
        {
          displayName: "",
          value: ""
        }
      )
      updateFields(this.fields, this.settings);
    }
    fields.appendChild(addButton);
  }
  
}

window.onload = () => {
  var fields = document.getElementById('fields');
  var saveButton = document.getElementById('save-button');
  var settings = {};

  updateFields(fields, settings);

  getJson('/settings_data')
      .then(data => {
        settings = data;
        updateFields(fields, settings);
      })
      .catch(data => console.log(data));

  saveButton.onclick = () => {
    postJson('/settings_data', settings)
        .then(data => console.log(data))
        .catch(data => console.log(data));
  };
}
