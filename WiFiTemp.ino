#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include <FS.h>
#include <ArduinoJson.h>
#include <Reflector.h>
#include <vector>

#define ONE_WIRE_BUS 2

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

class Topic {
public:
	String name;
  String value;
  
  Topic(const String& name, const String& value) : 
    name(name), value(value){
  }

	template<class Reflector>
	void reflect(Reflector& reflector) {
		reflector
		(name, "name", String("name"), "Name")
			(value, "value", String("value"), "Value");
	}
};

class TopicsWrapper {
public:
TopicsWrapper(std::vector<Topic>& topics): topics(topics) {}
std::vector<Topic>& topics;
template<class Reflector>
  void reflect(Reflector& reflector) {
      reflector
              (topics, "topics", std::vector<Topic>(), "Topics");
  }
};

class WiFiSettings{
public:
    String ssid;
    String password;

    template<class Reflector>
    void reflect(Reflector& reflector) {
        reflector
                (ssid, "ssid", String("ssid"), "SSID")
                (password, "password", String("password"), "Password");
    }
};

class Settings {
  const char *  fileName = "/config.json";
public:
  WiFiSettings wiFi;
  int sendInterval = 60;
  String postUrl;
  std::vector<String> topics;
  

    template<class Reflector>
    void reflect(Reflector& reflector) {
        reflector
                (wiFi, "wiFi", WiFiSettings(), "WiFi")
                (sendInterval, "sendInterval", 60, "Send Interval")
                (postUrl, "postUrl", String("http://192.168.0.104/topics.json"), "Post Url")
                (topics, "topics",  {"North", "N.Room"}, "Topics");
    }

    void load () {
        File file = SPIFFS.open(fileName, "r");
        DynamicJsonBuffer jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(file);
        JsonDeserializer<JsonObject> deserializer(root);
        reflect(deserializer);
        file.close();
    }

    void save () {
      File file = SPIFFS.open(fileName, "w");
      DynamicJsonBuffer jsonBuffer;
      JsonObject& root = jsonBuffer.createObject();
      JsonSerializer<JsonObject> serializer(root);
      reflect(serializer);
      root.printTo(file);
      file.close();
    }
};

Settings settings;

ESP8266WebServer server(80);

bool connectToSsid() {
  
    String text = "Connecting to\n" +
      settings.wiFi.ssid;
  
    WiFi.mode(WIFI_STA);
    WiFi.begin(settings.wiFi.ssid.c_str(), settings.wiFi.password.c_str());
  
    Serial.println(text);
    int counter = 10;
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      text += ".";
      Serial.println(text);
      counter--;
      if(counter == 0) {
        WiFi.disconnect();
        return false;
      }
    }
  
    text = "Connected\n" + WiFi.localIP().toString();
    Serial.println(text);
    delay(1000);
    return true;
  }

bool startAp() {
  Serial.println("Try to start ap\n" + settings.wiFi.ssid + "\npass:\n" + settings.wiFi.password);
  boolean result = WiFi.softAP(settings.wiFi.ssid.c_str(), settings.wiFi.password.c_str());
  if(result == true)
  {
    IPAddress myIP = WiFi.softAPIP();
    Serial.println("AP IP address:\n" + myIP.toString());
    delay(1000);
    return true;
  }
  Serial.println(String("AP Failed!"));
  delay(1000);
  return false;
}

std::vector<float> getTemps() {
  std::vector<float> result;
  int devices = sensors.getDeviceCount();
  if (devices == 0) {
    return result;
  }
  sensors.requestTemperatures(); 
  while(!sensors.isConversionComplete()) {
  }
  for(int i = 0; i < devices; i++) {
    float temp = sensors.getTempCByIndex(i);
    result.push_back(temp);
  }
  return result;
}

void getSettingsData() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonMetaSerializer<JsonObject> serializer(root);
  settings.reflect(serializer);
  String response;
  root.printTo(response);
  server.send(200, "application/json", response);
}

void postSettingsData() {
  String plain = server.arg("plain");
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parse(plain);
  JsonMetaDeserializer<JsonObject> deserializer(root);
  settings.reflect(deserializer);
  settings.save();
  getSettingsData();
}

int sendMessageCount = 0;

void messagePublished() {
  sendMessageCount--;
  Serial.println("Published");
}

bool sendTemperatures(std::vector<float> temps) {
  HTTPClient http;
  Serial.println("Post to " + settings.postUrl);
  http.begin(settings.postUrl);
  std::vector<Topic> topics;
  String payload;
  for(int i = 0; i < settings.topics.size() && i < temps.size(); i++) {
    String name = settings.topics[i];
    float value = temps[i];
    topics.push_back({name, String(value) + "C"});
  }
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonSerializer<JsonObject> serializer(root);
  TopicsWrapper wrapper(topics);
  wrapper.reflect(serializer);
  String response;
  root.printTo(payload);
  root.printTo(Serial);
  int returnCode = 0;
  int attemps = 5;
  while(returnCode != 200 && attemps > 0) {
    Serial.println("Try");
    returnCode = http.POST(payload);
    Serial.println("Return code " + String(returnCode));
    attemps--;
  }
  return returnCode == 200;
}

void sendToServer() {
  std::vector<float> temps = getTemps();
  Serial.println("Temps Count" + String(temps.size()));
  bool result = sendTemperatures(temps);
  server.send(200, "text/html", String(result));
}

void setup() {
  SPIFFS.begin();
  Serial.begin(115200);
  settings.load();
  if (!connectToSsid()) {
     startAp();
  }
  sensors.begin();

  int devices = sensors.getDeviceCount();
  if (devices != 0) {
    std::vector<float> temps = getTemps();
    sendTemperatures(temps);
    int interval = settings.sendInterval;
    ESP.deepSleep(1e6 * interval);
  }

  server.on("/settings_data", HTTP_POST, postSettingsData);
  server.on("/settings_data", HTTP_GET, getSettingsData);
  server.on("/send", HTTP_POST, sendToServer);

  server.serveStatic("/", SPIFFS, "/index.html");
  server.serveStatic("/index.js", SPIFFS, "/index.js");
  server.begin();
}

void loop() {
  server.handleClient();
}




